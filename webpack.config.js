const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
  devServer: {
     contentBase: './dist',
  },
  module: {
  	rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      ]
    },
  plugins: [
  new HtmlWebpackPlugin({
    title: 'Custom template',
    // Load a custom template (lodash by default)
    template: './src/index.html'
 	 })
	],

};
