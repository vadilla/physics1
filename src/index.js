import ApexCharts from 'apexcharts'
import './style.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';
import $ from 'jquery'

const canvas = document.getElementById("canvas");
const ctx = canvas.getContext('2d');
const playButton = document.getElementById("start");
const resetButton = document.getElementById("reset");
const angleSlider = document.getElementById("slider");
const angleLabel = document.getElementById("slider-label");
const tensionInput = document.getElementById("tension");
const chartSwitch = document.getElementById("chartSwitch");
const vectorSwitch = document.getElementById("vectorSwitch");
const nav = document.getElementById("myTab");

ctx.canvas.height = canvas.offsetHeight;
ctx.canvas.width = canvas.offsetWidth;


window.addEventListener("resize", ev => {
    ctx.canvas.height = canvas.offsetHeight;
    ctx.canvas.width = canvas.offsetWidth;
});

angleSlider.addEventListener("input", ev => {
    angle = (parseFloat(ev.target.value)) / 180 * Math.PI;
    angleLabel.innerText = "Угол наклона поверхности " + ev.target.value + "°";
    drawAll();
});

tensionInput.addEventListener("input", ev => {
    u = parseFloat(ev.target.value);
    drawAll();
});

chartSwitch.addEventListener("change", ev => {
    showChart = !showChart;
    updateSeries();
});

vectorSwitch.addEventListener("change", ev => {
    showVector = !showVector;
    drawAll();
})

nav.addEventListener("click", ev => {
    // updateSeries();
});

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    updateSeries();
})

playButton.addEventListener("click", ev => {
    updateSeries();
    updateData();
    if (animationId == null) {
        play();
    } else {
        pause();
    }
})

resetButton.addEventListener("click", ev => {
    t = 0;
    angleSlider.disabled = false;
    tensionInput.disabled = false;
    playButton.disabled = false;
    accData = [];
    speedData = [];
    positionData = [];
    updateSeries();
    pause();
    drawAll();
})



let angle = Math.PI / 6;
let lastTime = performance.now();
let animationId = null;
let t = 0;
let g = 9.81;
let u = 0.3;
let m = 10;
let showChart = false;
let showVector = false;
let accData = [];
let speedData = [];
let positionData = [];

const getGravityForce = () => {
    return m * g;
}

const getForce = () => {
    return getGravityForce() * Math.sin(angle);
}

const getNormalForce = () => {
    return getGravityForce() * Math.cos(angle);
}

const getTensionForce = () => {
    return -u * getNormalForce();
}
const getA = () => {
    return (getForce() + getTensionForce()) / m;

}
const getV = t => {
    return getA() * t;
}

const getX = t => {
    return getA() * t * t / 2;
}

const play = () => {
    lastTime = performance.now();
    playButton.textContent = "Пауза";
    animationId = requestAnimationFrame(renderLoop);
    tensionInput.disabled = true;
    angleSlider.disabled = true;

    accChart.updateOptions({
        xaxis: {max: getMaxT()},
        yaxis: {
            min: 0,
            max: Math.ceil(2 * getA()),
            decimalsInFloat: 2,
            title: {
                text: "a(м/с^2)"
            },
        }
    });
    speedChart.updateOptions({
        xaxis: {max: getMaxT()},
        yaxis: {
            min: 0,
            max: Math.ceil(getV(getMaxT())),
            decimalsInFloat: 2,
            title: {
                text: "v(м/с)"
            },
        }
    });
    positionChart.updateOptions({
        xaxis: {max: getMaxT()},
        yaxis: {
            min: 0,
            max: Math.ceil(getX(getMaxT())),
            decimalsInFloat: 2,
            title: {
                text: "x(м)"
            },
        }
    });
}

const pause = () => {
    playButton.textContent = "Старт";
    cancelAnimationFrame(animationId);
    animationId = null;
}

const updateSeries = () => {
    accChart.updateSeries([{
        data: accData
    }]);
    speedChart.updateSeries([{
        data: speedData
    }]);
    positionChart.updateSeries([{
        data: positionData
    }]);
}

const drawVectors = (x, y) => {
    ctx.save()
    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.rotate(angle);
    ctx.translate(-canvas.width / 2, -canvas.height / 2);

    if (angle !== 0 && angle !== Math.PI / 2) {
        drawArrow(ctx, new Point(x, y), new Point(x + getForce(), y), 5, "#000000", "Fx");
        drawArrow(ctx, new Point(x, y), new Point(x, y + getNormalForce()), 5, "#000000", "Fy");
    }
    drawArrow(ctx, new Point(x, y), new Point(x + getTensionForce(), y), 5, "#000000", "F тр.");
    drawArrow(ctx, new Point(x, y), new Point(x, y - getNormalForce()), 5, "#000000", "N");
    ctx.save();
    ctx.translate(x, y);
    ctx.rotate(-angle);
    drawArrow(ctx, new Point(0, 0), new Point(0, getGravityForce()), 5, "#000000", "mg");
    ctx.restore();
    ctx.restore();
}

const drawAll = () => {
    let x0 = Math.floor(Math.min(canvas.width / (2 * Math.cos(angle)), canvas.height / (2 * Math.sin(angle))) - canvas.width / 2 - 30);
    let x = -x0 + 100 * getX(t);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.save()
    ctx.translate(canvas.width / 2, canvas.height / 2);
    ctx.rotate(angle);
    ctx.translate(-canvas.width / 2, -canvas.height / 2);

    ctx.fillStyle = "#ff9e53";
    ctx.fillRect(-canvas.width, canvas.height / 2, 3 * canvas.width, 2 * canvas.height);
    ctx.fillStyle = "#7f4680";
    ctx.strokeRect(-canvas.width, canvas.height / 2, 3 * canvas.width, 2 * canvas.height);

    ctx.fillStyle = "#8cae5c"
    ctx.fillRect(x, canvas.height / 2, 50, -30);
    ctx.fillStyle = "#4a4a4a";
    ctx.strokeRect(x, canvas.height / 2, 50, -30);

    ctx.fillStyle = "#000000";
    for (let lx = -x0; lx < -x0 + canvas.width; lx += 10) {
        ctx.fillRect(lx, canvas.height / 2, 1, 10);
    }
    ctx.restore();

    if (showVector) {
        drawVectors(x + 25, canvas.height / 2 - 15);
    }

    // drawArrow(ctx, new Point(20, 10), new Point(50, 50), 7, "#f3f3f3");

}

class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

function drawArrow(context, from, to, radius, color, text) {
    var x_center = to.x;
    var y_center = to.y;

    var angle;
    var x;
    var y;

    context.beginPath();
    context.fillStyle = color;

    angle = Math.atan2(to.y - from.y, to.x - from.x)
    x = radius * Math.cos(angle) + x_center;
    y = radius * Math.sin(angle) + y_center;

    context.moveTo(x, y);

    angle += (1.0/3.0) * (2 * Math.PI)
    x = radius * Math.cos(angle) + x_center;
    y = radius * Math.sin(angle) + y_center;

    context.lineTo(x, y);

    angle += (1.0/3.0) * (2 * Math.PI)
    x = radius *Math.cos(angle) + x_center;
    y = radius *Math.sin(angle) + y_center;

    context.lineTo(x, y);
    context.closePath();
    context.fill();

    context.beginPath();
    context.lineWidth = radius / 2;
    context.moveTo(from.x, from.y);
    context.lineTo(to.x, to.y);
    context.strokeStyle = color;
    context.stroke();
    context.lineWidth = 1;
    context.closePath();

    let textX = to.x, textY = to.y;
    ctx.save();
    context.fillStyle = "#000000";
    ctx.translate(textX, textY);
    ctx.rotate(Math.atan2(-(from.y - to.y), -(from.x - to.x)) % (Math.PI));
    ctx.translate(0, -7);
    ctx.rotate(-Math.atan2(-(from.y - to.y), -(from.x - to.x)) % (Math.PI));
    ctx.rotate(Math.atan2(-(from.y - to.y), -(from.x - to.x)) % (Math.PI));
    ctx.translate(-textX, -textY);
    ctx.fillText(text, textX, textY);
    ctx.restore();

}

const getMaxT = () => {
    let x0 = (2 * Math.floor(Math.min(canvas.width / (2 * Math.cos(angle)), canvas.height / (2 * Math.sin(angle)))) - 80) / 100;
    return Math.sqrt(2 * x0 / getA());
}

const renderLoop = () => {
    let dt = (performance.now() - lastTime) / 1000;
    t += dt;

    drawAll();
    lastTime = performance.now();
    if (t < getMaxT()) {
        animationId = requestAnimationFrame(renderLoop);
    } else {
        playButton.disabled = true;
        pause();
    }
};

drawAll();
pause();


function updateData() {
    accData.push({x: t, y: getA(t)});
    speedData.push({x: t, y: getV(t)});
    positionData.push({x: t, y: getX(t)});
}

let options = {
    chart: {
        id: 'realtime',
        height: 350,
        type: 'line',
        animations: {
            enabled: true,
            easing: 'linear',
            dynamicAnimation: {
                speed: 1000
            }
        },
        toolbar: {
            show: false
        },
        zoom: {
            enabled: false
        }
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        curve: 'straight'
    },
    markers: {
        size: 0
    },
    xaxis: {
        type: 'numeric',
        min: 0,
        title: {
            text: "t(c)",
            offsetY: 10,
        }
    },
    legend: {
        show: false
    },
};

let accOptions = {
    series: [{
        data: accData.slice()
    }],
    yaxis: {
        min: 0,
        title: {
            text: "a(м/с^2)"
        },
    },
    ...options
};
let speedOptions = {
    series: [{
        data: speedData.slice()
    }],
    yaxis: {
        min: 0,
        title: {
            text: "v(м/с)"
        },
    },
    ...options
};
let positionOptions = {
    series: [{
        data: positionData.slice()
    }],
    yaxis: {
        min: 0,
        title: {
            text: "x(м)"
        },
    },
    ...options
}


let positionChart = new ApexCharts(document.querySelector("#position-chart"), positionOptions);
let speedChart = new ApexCharts(document.querySelector("#speed-chart"), speedOptions);
let accChart = new ApexCharts(document.querySelector("#acc-chart"), accOptions);


positionChart.render();
speedChart.render();
accChart.render();

window.setInterval(function () {
    if (animationId != null) {
        updateData();
        if (showChart) {
            updateSeries();
        }
    }
}, 50)





